const Entry = require("./entry");

const path = require('path');
const webpack = require("webpack");

const entries = Entry.Entry.entriesWithDir();
const config = {
    entry: entries,
    output: {
        path: path.resolve(__dirname, "./public/javascripts"),
        filename: "[name].min.js"
    },
    resolve: {
        extensions: [".webpack.js", ".web.js", ".ts", ".js"]
    }
};

module.exports = config;
