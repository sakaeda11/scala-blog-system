module.exports.Entry = class Entry {
    static entries() {
        return {
            vendor: "common/vendor.ts",
            polyfills: "common/polyfills/polyfills.ts",

            "bundle": "modules/app.module.ts",
            "bundle-admin": "modules/app-admin.module.ts",
        };
    };

    static entriesWithDir() {
        const entries = Entry.entries();
        const entriesWithDir = {};
        Object.keys(entries).forEach((key) => {
            entriesWithDir[key] = "./app_front/" + entries[key];
        });
        return entriesWithDir;
    }
}