const merge = require('webpack-merge');
const baseConfig = require('./webpack.config.base.js');
const webpack = require("webpack");

const config = merge(baseConfig, {
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /(node_modules)/,
                use: [
                    {
                        loader: "babel-loader"
                    },
                    {
                        loader: "ts-loader"
                    }
                ]
            },
            {
                test: /\.json$/,
                use: "json-loader"
            }
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: false,
            warnings: false
        })
    ]
});

module.exports = config;