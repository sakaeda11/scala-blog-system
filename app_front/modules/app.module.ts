import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {AppComponent} from "../components/app.component";
import {AppRoutingModule} from "./app.routing.module";
import {AppTopComponent} from "../components/app-top.component";
import {
    DateAdapter,
    MdButtonModule,
    MdCardModule,
    MdDatepickerModule,
    MdInputModule,
    MdNativeDateModule,
    MdRadioModule,
    MdSelectModule
} from "@angular/material";
import {FormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {SimpleDateAdapter} from "../common/SimpleDateAdapter";
import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";

@NgModule({
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
        AppTopComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        MdDatepickerModule,
        MdNativeDateModule,
        MdButtonModule,
        MdSelectModule,
        MdInputModule,
        MdRadioModule,
        MdCardModule,
    ],
    providers: [
        {
            provide: DateAdapter,
            useClass: SimpleDateAdapter,
        },
    ],
})
export class AppModule {
}


platformBrowserDynamic().bootstrapModule(AppModule);
