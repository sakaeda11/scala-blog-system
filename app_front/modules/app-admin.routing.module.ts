import {NgModule} from '@angular/core';
import {AppAdminComponent} from "../components/app-admin.component";
import {RouterModule, Routes} from "@angular/router";


const routes: Routes = [
    {
        component: AppAdminComponent,
        path: "",
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppAdminRoutingModule {
}
