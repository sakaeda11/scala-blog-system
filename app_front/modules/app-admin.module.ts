import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {AppAdminComponent} from "../components/app-admin.component";
import {AppAdminRoutingModule} from "./app-admin.routing.module";
import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule} from "@angular/forms";
import {
    DateAdapter,
    MdButtonModule, MdCardModule, MdDatepickerModule, MdInputModule, MdNativeDateModule,
    MdRadioModule, MdSelectModule
} from "@angular/material";
import {SimpleDateAdapter} from "../common/SimpleDateAdapter";

@NgModule({
    bootstrap: [AppAdminComponent],
    declarations: [
        AppAdminComponent,
    ],
    imports: [
        BrowserModule,
        HttpModule,
        AppAdminRoutingModule,
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        MdDatepickerModule,
        MdNativeDateModule,
        MdButtonModule,
        MdSelectModule,
        MdInputModule,
        MdRadioModule,
        MdCardModule,
    ],
    providers: [
        {
            provide: DateAdapter,
            useClass: SimpleDateAdapter,
        },
    ],
})
export class AppAdminModule {
}

platformBrowserDynamic().bootstrapModule(AppAdminModule);