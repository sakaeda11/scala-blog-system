import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";

@Component({
    selector: "angular-root",
    styles: [`
    `],
    template: `    
        aaa
        <router-outlet></router-outlet>
    `,
})
export class AppComponent implements OnInit {
    constructor(private router: Router) {
    }

    public ngOnInit() {
    }
}