import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";

@Component({
    selector: "angular-root",
    styles: [`
    `],
    template: `
        admin
        <router-outlet></router-outlet>
    `,
})
export class AppAdminComponent implements OnInit {
    constructor(private router: Router) {
    }

    public ngOnInit() {
    }
}