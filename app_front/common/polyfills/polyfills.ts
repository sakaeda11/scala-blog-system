import "core-js";
import "web-animations-js";
import "./classList";
import "./typedarray";
import "./Blob";
import "./formdata";